# README #

Quick Coding Test Readme.

### What is this repository for? ###

* Write a simple app to display data about books for sale on eBay.
* Version 1

### Instructions ###

Write a simple app to display data about books for sale on eBay. Fetch the data from
this URL:
[http://de-coding-test.s3.amazonaws.com/books.json](Link URL)

* Once you set up your environment we ask that you spend around two hours to
complete and submit your project.
* You can use external libraries but we ask that you try to limit the use of them as
much as possible. (Our developers would look at 1-2 for this project)
* You may submit your project before two hours have elapsed, but please remember
that we’re focused on quality and not on speed.
* You may only submit your project once. Resubmissions will not be accepted.
* If you are not completely finished after the two hours have elapsed, submit your
unfinished project, as we would like to review.
Once you’ve submitted your project, send us a separate follow-up email with a brief
summary detailing any modifications or additions you would make to your code if you
were given more time to develop.

## We’re looking for submissions that: ##
* Display ALL of the data in the list we’ve provided,
including the thumbnail images.
* Fetch the data over the network, and don’t simply
bundle it into the app.
* Feature clean and consistent code.
* Demonstrate a solid understanding of the Android
Platform.

## Contact Info ##
Joseph Hopson
jh@josephhopson.com
937-272-7424