package com.josephhopson.ebaybooks.presentation;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.josephhopson.ebaybooks.R;

/**
 * Created by Joseph Hopson on 5/26/2016.
 */
public class WaitingDialog extends DialogFragment {

    public static final String WAITING_TEXT_BUNDLE_KEY = "WAITING_TEXT";
    TextView waitingMessageTextView;

    public static WaitingDialog newInstance(String waitingText) {
        WaitingDialog fragment = new WaitingDialog();
        Bundle args = new Bundle();
        args.putString(WAITING_TEXT_BUNDLE_KEY, waitingText);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialogView = inflater.inflate(R.layout.dialogfrag_wait_spinner, container);
        waitingMessageTextView = (TextView) dialogView.findViewById(R.id.waiting_message);
        if(!TextUtils.isEmpty(getArguments().getString(WAITING_TEXT_BUNDLE_KEY))) {
            waitingMessageTextView.setText(getArguments().getString(WAITING_TEXT_BUNDLE_KEY));
        }
        setCancelable(false);
        return dialogView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

}
