package com.josephhopson.ebaybooks.presentation;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.josephhopson.ebaybooks.R;
import com.josephhopson.ebaybooks.model.Book;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;

/**
 * A fragment representing a single Book detail screen.
 * This fragment is either contained in a {@link BookListActivity}
 * in two-pane mode (on tablets) or a {@link BookDetailActivity}
 * on handsets.
 */
public class BookDetailFragment extends Fragment {

    public static final String ARG_BOOK_ID = "item_id";

    private Realm realm;

    private TextView title;
    private TextView author;
    private ImageView cover;

    public BookDetailFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        if (getArguments().containsKey(ARG_BOOK_ID)) {
            // get book
            RealmQuery<Book> query = realm.where(Book.class);
            query.equalTo("title", getArguments().getString(ARG_BOOK_ID));
            query.findFirstAsync().addChangeListener(new RealmChangeListener<Book>() {
                @Override
                public void onChange(Book book) {
                    loadBook(book);
                }
            });

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(getArguments().getString(ARG_BOOK_ID));
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.book_detail, container, false);

        title = ((TextView) rootView.findViewById(R.id.title));
        author = ((TextView) rootView.findViewById(R.id.author));
        cover = ((ImageView) rootView.findViewById(R.id.cover));

        return rootView;
    }

    private void loadBook(Book book) {
        if(!TextUtils.isEmpty(book.getImageURL())) {
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                ImageView iv = (ImageView) appBarLayout.findViewById(R.id.header_image);
                if(iv != null) {
                    Glide.with(getActivity())
                            .load(book.getImageURL())
                            .into(iv);
                }
            }
            if(cover != null) {
                Glide.with(getActivity())
                        .load(book.getImageURL())
                        .into(cover);
            }
        }

        if(title != null) {
            title.setText(book.getTitle());
        }
        if(author != null) {
            author.setText(book.getAuthor());
        }
    }
}
