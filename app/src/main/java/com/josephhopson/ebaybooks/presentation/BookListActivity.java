package com.josephhopson.ebaybooks.presentation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.josephhopson.ebaybooks.R;
import com.josephhopson.ebaybooks.model.Book;
import com.josephhopson.ebaybooks.network.Constants;
import com.josephhopson.ebaybooks.network.EbayApiInterface;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmRecyclerViewAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * An activity representing a list of Books. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link BookDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class BookListActivity extends AppCompatActivity {

    private static final String TAG = "BookListActivity";

    private boolean mTwoPane;

    private Realm realm;
    private WaitingDialog mWaitingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);
        mWaitingDialog = WaitingDialog.newInstance(getString(R.string.dialog_loading_books_text));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        if (findViewById(R.id.book_detail_container) != null) {
            mTwoPane = true;
        }

        // Networking and data setup
        realm = Realm.getDefaultInstance();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        EbayApiInterface mBooksService = retrofit.create(EbayApiInterface.class);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.book_list);
        recyclerView.setAdapter(new BooksRecyclerAdapter(getApplicationContext(), realm.where(Book.class).findAllAsync()));

        // Since this is a one time GET, check if you already have data.
        if(realm.isEmpty()) {
            // get the data async
            setWaitScreen(true);
            Observable<RealmList<Book>> call = mBooksService.getBooks();
            call.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(booksSubscriber);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onPause() {
        if (mWaitingDialog != null && mWaitingDialog.isVisible()) {
            mWaitingDialog.dismiss();
        }
        super.onPause();
    }

    void setWaitScreen(boolean set) {
        if (set) {
            if (mWaitingDialog != null && !BookListActivity.this.isFinishing()) {
                mWaitingDialog.show(getSupportFragmentManager().beginTransaction(), null);
            }
        } else {
            if (mWaitingDialog != null && !BookListActivity.this.isFinishing()) {
                mWaitingDialog.dismiss();
            }
        }
    }

    private Subscriber<RealmList<Book>> booksSubscriber =
            new Subscriber<RealmList<Book>>() {

                @Override
                public void onCompleted() {
                    setWaitScreen(false);
                }

                @Override
                public void onError(Throwable e) {
                    complain(e.getMessage());
                }

                @Override
                public void onNext(final RealmList<Book> books) {
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(books);
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            // YAY!
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            complain(error.getMessage());
                        }
                    });
                }
            };

    void complain(String message) {
        Log.e(TAG, message);
        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setNeutralButton("OK", null)
                .create()
                .show();
    }


    // -----------------
    // Books Adapter
    // -----------------

    public class BooksRecyclerAdapter extends RealmRecyclerViewAdapter<Book, BookViewHolder> {

        public BooksRecyclerAdapter(Context context, OrderedRealmCollection<Book> data) {
            super(context, data, true);
        }

        @Override
        public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_list_content, parent, false);
            return new BookViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final BookViewHolder holder, int position) {
            final Book book = getData().get(position);
            holder.mName.setText(book.getTitle());
            // author
            if(!TextUtils.isEmpty(book.getAuthor())) {
                holder.mAuthor.setVisibility(View.VISIBLE);
                holder.mAuthor.setText(book.getAuthor());
            } else {
                holder.mAuthor.setText("");
                holder.mAuthor.setVisibility(View.GONE);
            }
            // image
            if(!TextUtils.isEmpty(book.getImageURL())) {
                holder.mBookCover.setVisibility(View.VISIBLE);
                Glide.with(getApplicationContext())
                        .load(book.getImageURL())
                        .error(android.R.drawable.ic_menu_report_image)
                        .into(holder.mBookCover);
            } else {
                holder.mBookCover.setVisibility(View.GONE);
            }
            // clicky
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(BookDetailFragment.ARG_BOOK_ID, book.getTitle());
                        BookDetailFragment fragment = new BookDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.book_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, BookDetailActivity.class);
                        intent.putExtra(BookDetailFragment.ARG_BOOK_ID, book.getTitle());
                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}
