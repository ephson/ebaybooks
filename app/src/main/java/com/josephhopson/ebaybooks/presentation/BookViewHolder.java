package com.josephhopson.ebaybooks.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.josephhopson.ebaybooks.R;

/**
 * Created by Joseph Hopson on 7/6/2016.
 */
public class BookViewHolder extends RecyclerView.ViewHolder {

    public final View mView;
    public final TextView mName;
    public final TextView mAuthor;
    public final ImageView mBookCover;

    public BookViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mName = (TextView) itemView.findViewById(R.id.name);
        mAuthor = (TextView) itemView.findViewById(R.id.author);
        mBookCover = (ImageView) itemView.findViewById(R.id.cover);
    }
}
