package com.josephhopson.ebaybooks.network;

import com.josephhopson.ebaybooks.model.Book;

import io.realm.RealmList;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Joseph Hopson on 7/6/2016.
 */
public interface EbayApiInterface {

    @GET ("books.json")
    Observable<RealmList<Book>> getBooks();
}
