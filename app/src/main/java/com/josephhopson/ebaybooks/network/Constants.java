package com.josephhopson.ebaybooks.network;

/**
 * Created by Joseph Hopson on 7/6/2016.
 */
public class Constants {
    private Constants() {}

    public static final String BASE_URL = "http://de-coding-test.s3.amazonaws.com/";
}
