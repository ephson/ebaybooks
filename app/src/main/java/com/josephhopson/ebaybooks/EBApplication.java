package com.josephhopson.ebaybooks;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Joseph Hopson on 7/6/2016.
 */
public class EBApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext())
                .name("ebayBooksRealm.realm")
                .schemaVersion(1)
                .build();

        if(BuildConfig.DEBUG) {
            Realm.deleteRealm(config);
        }

        Realm.setDefaultConfiguration(config);
    }
}
